﻿namespace EsperPodstawy
{
    using System;

    // KursAkcji.java
    public class StockPrice
    {
        public StockPrice(string corporation, string market, DateTime date, float openingPrice, float maxValue, float minValue, float closingPrice, float tradingVolume)
        {
            this.Corporation = corporation;
            this.Market = market;
            this.Date = date;
            this.OpeningPrice = openingPrice;
            this.MaxValue = maxValue;
            this.MinValue = minValue;
            this.ClosingPrice = closingPrice;
            this.TradingVolume = tradingVolume;
        }

        #region Properties
        // spolka
        public string Corporation { get; private set; }

        // market
        public string Market { get; private set; }

        // data
        public DateTime Date { get; private set; }

        // kursOtwarcia
        public float OpeningPrice { get; private set; }

        // wartoscMax
        public float MaxValue { get; private set; }

        // wartoscMin
        public float MinValue { get; private set; }

        // kursZamkniecia
        public float ClosingPrice { get; private set; }

        // obrot
        public float TradingVolume { get; private set; }
        #endregion

        public override string ToString()
        {
            return "KursAkcji   [spolka=" + this.Corporation + ",\tmarket=" + this.Market + ",\tdata="
                   + this.Date.ToString("yyyy-MMM-dd") + ",\tkursOtwarcia=" + this.OpeningPrice + ",\twartoscMax="
                   + this.MaxValue + ",\twartoscMin=" + this.MinValue + ",\tkursZamkniecia=" + this.ClosingPrice
                   + ",\tobrot=" + this.TradingVolume + "]";
        }
    }
}
